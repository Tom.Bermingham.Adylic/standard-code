//  SUMMARY
//  Provides an easy way of using an API in Apps Script.
//  Tracks and logs the URL, response time, and response code.
//
//  API USAGE
//  const NewApi = new ApiService(BASE_URL, TOKEN);
//  const res = NewApi.GET(ENDPOINT);
//
//  GENERIC FETCH
//  const res = ApiService.fetch(FULL_URL);


//  KNOWN ISSUES
//  Only has functions for GET and POST.
//  Only works with json content type. Would not work for APIs that return XML.
//  Only form of authentication is: Authorization: 'Bearer token'.



class ApiService {
    //Each instance of ApiService connects to a specific API
    constructor(baseUrl, token) {
      this.baseUrl = baseUrl;
      this.token = token;
    }
  
    //Define common options for requests (GET and POST)
    GET(endpoint) { return this.fetch(endpoint, { method: 'GET' }) };
    POST(endpoint, payload) { return this.fetch( endpoint, { method: 'POST', contentType: "application/json", payload: JSON.stringify(payload) } ) };
    
    //Provide API specific fetch
    fetch(endpoint, options) {
      const url = this.baseUrl + endpoint;
      const headers = { Authorization: `Bearer ${this.token}` };
  
      const startTime = new Date().getTime();
      const response = UrlFetchApp.fetch(url, {
        ...options,
        headers,
        muteHttpExceptions: true,
      });
      Logger.log(`URL: ${url}\nResponse Time: ${new Date().getTime() - startTime} ms\nResponse Code: ${response.getResponseCode()}`);
      
      try {
        return JSON.parse(response.getContentText());
      } catch (error) {
        return response.getContentText();
      }
    };
  
    //Provide ability to fetch a generic URL (not from an API)
    static fetch(url) {
      const startTime = new Date().getTime();
      const response = UrlFetchApp.fetch(url);
      Logger.log(`URL: ${url}\nResponse Time: ${new Date().getTime() - startTime} ms\nResponse Code: ${response.getResponseCode()}`);
      return response;
    }
  }
  
  
  
  
  
  
  
  